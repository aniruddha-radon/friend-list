app.controller("friendListController", function ($scope) {
    $scope.friends = [{
        name: 'Harry Potter',
        age: 35,
        contact: '0123456789',
        address: 'Cupboard under the stair case, Privet Drive, Little Whinging',
        dob: '31st July 1981',
        favoriteColor: 'Purple',
        showDetails: false
    }, {
        name: 'Ash Ketchum',
        age: 10,
        contact: '0987654321',
        address: 'Upper House, Pallet Town',
        dob: '1st April 1997',
        favoriteColor: 'Yellow',
        showDetails: false
    },  {
        name: 'Optimus Prime',
        age: 5000000,
        contact: '0101010101',
        address: 'Cybertron',
        dob: 'Unknown',
        favoriteColor: 'Blue',
        showDetails: false
    }];


    $scope.showDetails = function(index) {
        $scope.friends[index].showDetails = true;
    }
})